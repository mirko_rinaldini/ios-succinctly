//
//  ViewController.m
//  AssetManagement
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSFileManager *sharedFM = [NSFileManager defaultManager];
    NSArray *paths = [sharedFM URLsForDirectory:NSLibraryDirectory  inDomains:NSUserDomainMask];
    
    if ([paths count] > 0) {
        NSString *documentsPath = [paths[0] path]; NSLog(@"%@", documentsPath);
        NSDirectoryEnumerator *enumerator = [sharedFM enumeratorAtPath:documentsPath];
        id object;
        while(object = [enumerator nextObject]) {
            NSLog(@"%@", object);
        }
    }
}
                                                                                             

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
