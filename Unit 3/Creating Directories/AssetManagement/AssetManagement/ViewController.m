//
//  ViewController.m
//  AssetManagement
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSFileManager *sharedFM = [NSFileManager defaultManager]; NSArray *paths = [sharedFM URLsForDirectory:NSLibraryDirectory  inDomains:NSUserDomainMask];
    
    if ([paths count] > 0) {
    NSURL *libraryPath = paths[0];
    NSURL *templatesPath = [libraryPath   URLByAppendingPathComponent:@"Templates"];
    NSError *error = nil;
    BOOL success = [sharedFM createDirectoryAtURL:templatesPath
                    withIntermediateDirectories:YES attributes:nil  error:&error];
        
    if (success) {
                NSLog(@"Successfully created a directory at %@",templatesPath);                                                           
            } else {
            NSLog(@"Could not create the directory. Error: %@", error);
            }
    }
}
                                                                                             

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
