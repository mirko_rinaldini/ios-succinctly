//
//  ViewController.m
//  AssetManagement
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSFileManager *sharedFM = [NSFileManager defaultManager]; NSArray *paths = [sharedFM URLsForDirectory:NSLibraryDirectory  inDomains:NSUserDomainMask];
   
      if ([paths count] > 0) {
                NSURL *libraryPath = paths[0];
                NSURL *appDataPath = [libraryPath URLByAppendingPathComponent:@"someData.txt"];
                NSError *error = nil;
                NSString *loadedText = [NSString stringWithContentsOfURL:appDataPath encoding:NSUnicodeStringEncoding error:&error];
                if (loadedText != nil) {
                    NSLog(@"Successfully loaded text: %@", loadedText);
                              }
                else {
                    NSLog(@"Could not load data from file. Error: %@", error);
                     }
      }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
