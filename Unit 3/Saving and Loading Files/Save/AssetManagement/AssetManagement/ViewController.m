//
//  ViewController.m
//  AssetManagement
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSFileManager *sharedFM = [NSFileManager defaultManager]; NSArray *paths = [sharedFM URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask];
    if ([paths count] > 0) {
        NSURL *libraryPath=paths[0];
        NSURL *appDataPath = [libraryPath URLByAppendingPathComponent:@"someData.txt"];
        NSString *someAppData = @"Hello, World! This is a file I created dynamically";
        NSError *error = nil;
        BOOL success = [someAppData writeToURL:appDataPath
                                    atomically:YES
                                      encoding:NSUnicodeStringEncoding
                                         error:&error];
        // Removing directory/files
//        ￼￼[sharedFM removeItemAtURL:targetURL error:&error];
        if (success) {
            NSLog(@"Wrote some data to %@", appDataPath);
        } else {NSLog(@"Could not write data to file. Error: %@", error);
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
