//
//  ViewController.m
//  Internalization
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [aButton setTitle:NSLocalizedString(@"Button Title", nil) forState:UIControlStateNormal];
    aButton.frame = CGRectMake(100.0, 200.0, 120.0, 40.0);
    [[self view] addSubview:aButton];
    [aButton addTarget:self
                action:@selector(sayHello:)
      forControlEvents:UIControlEventTouchUpInside];
}

- (void)sayHello:(id)sender {
    NSLog(@"%@", NSLocalizedString(@"Greeting", nil));
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
