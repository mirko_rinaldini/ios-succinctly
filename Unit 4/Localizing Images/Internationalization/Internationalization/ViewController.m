//
//  ViewController.m
//  Internationalization
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // Find the image.
    NSString *imagePath = [[NSBundle mainBundle]
                           pathForResource:@"syncfusion-icon"
                           ofType:@"png"];
    NSLog(@"%@", imagePath);
    // Load the image.
    UIImage *imageData = [[UIImage alloc]
                          initWithContentsOfFile:imagePath];
    if (imageData != nil) {
        // Display the image.
        UIImageView *imageView = [[UIImageView alloc]
                                  initWithImage:imageData]; CGRect screenBounds = [[UIScreen mainScreen] bounds];
        imageView.contentMode = UIViewContentModeCenter; CGRect frame = imageView.frame;
        frame.size.width = screenBounds.size.width; frame.size.height = screenBounds.size.height; imageView.frame = frame;
        [[self view] addSubview:imageView];
    } else {
        NSLog(@"Could not load the file"); }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
